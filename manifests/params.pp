# == Class: ticmonitor::params
#
#  Parameter class associated with TIC Monitor Module
#
# === Authors
#
# Russell McGregor <russell.mcgregor@brisbane.qld.gov.au>
#
# === Copyright
#
# Copyright 2014 Brisbane City Council, unless otherwise noted.
#

class ticmonitor::params {
  $db_host = 'itsdb'
  $db_name = 'ticweb'
  $db_user = undef
  $db_pass = undef
  $sched_interval = 10
  $net_timeout = 14
  $ssh_port = 22
  $thread_pool = 60
  $passwd_max_changes = 0
  $passwd_min_len = 6
  $passwd_max_len = 8
  $remote_reset_interval = 47
  $remote_reset_timeout = 14
  $remote_reset_retries = 5
  $remote_reset_delay = 42
  $remote_reset_socket_timeout = 7
  $ticmonitor_package = 'ticmonitor'
  $ticmonitor_service = 'ticmonitor'
}
