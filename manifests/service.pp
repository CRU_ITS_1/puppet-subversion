# == Class: subversion::service
#
#  Service class associated with subversion
#
# === Authors
#
# ITS <ITS@brisbane.qld.gov.au>
#
# === Copyright
#
# Copyright 2014 Brisbane City Council, unless otherwise noted.
#


class subversion::service {
  service { $subversion::params::subversion_service :
    ensure  => 'running',
    enable  => true,
    require => Package[$subversion::params::subversion_package],
  }
}
