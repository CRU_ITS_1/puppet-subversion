# == Class: subversion
#
# Deploy and Configure ITS subversion
#
# === Authors
#
# ITS <ITS@brisbane.qld.gov.au>
#
# === Copyright
#
# Copyright 2014 Brisbane City Council, unless otherwise noted.
#
class subversion (
  $db_host                      = $subversion::params::db_host,
  $db_name                      = $subversion::params::db_name,
  $db_user                      = $subversion::params::db_user,
  $db_pass                      = $subversion::params::db_pass,
  $sched_interval               = $subversion::params::sched_interval,
  $net_timeout                  = $subversion::params::net_timeout,
  $ssh_port                     = $subversion::params::ssh_port,
  $thread_pool                  = $subversion::params::thread_pool,
  $passwd_max_changes           = $subversion::params::passwd_max_changes,
  $passwd_min_len               = $subversion::params::passwd_min_len,
  $passwd_max_len               = $subversion::params::passwd_max_len,
  $remote_reset_interval        = $subversion::params::remote_reset_interval,
  $remote_reset_timeout         = $subversion::params::remote_reset_timeout,
  $remote_reset_retries         = $subversion::params::remote_reset_retries,
  $remote_reset_delay           = $subversion::params::remote_reset_delay,
  $remote_reset_socket_timeout  = $subversion::params::remote_reset_socket_timeout,
  $subversion_package           = $subversion::params::subversion_package,
  $subversion_service           = $subversion::params::subversion_service,

) inherits subversion::params {  
  include stdlib

  anchor { 'subversion::begin': }
  -> class { 'subversion::package': }
  -> class { 'subversion::config': }
  ~> class { 'subversion::service': }
  -> anchor { 'subversion::end': }
}
