# == Class: subversion::package
#
#  Package class associated with TIC subversion
#
# === Authors
#
# ITS <ITS@brisbane.qld.gov.au>
#
# === Copyright
#
# Copyright 2014 Brisbane City Council, unless otherwise noted.
#


class subversion::package {
  package { $subversion::params::subversion_package:
    ensure => 'present',
    install_options => ['--force-yes','--force-confmiss'],
  }
}
