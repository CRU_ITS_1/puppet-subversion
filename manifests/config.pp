# == Class: subversion::config
#
#  Configuration class associated with subversion
#
# === Authors
#
# ITS <ITS@brisbane.qld.gov.au>
#
# === Copyright
#
# Copyright 2014 Brisbane City Council, unless otherwise noted.
#

class subversion::config {
  file { '/etc/subversion/subversion.properties':
    ensure  => 'file',
    owner   => 'subversion',
    group   => 'subversion',
    mode    => '0600',
    content => template('subversion/subversion.properties.erb'),
    }
}

# At some stage we need to configure the logback and log4j config files here too.