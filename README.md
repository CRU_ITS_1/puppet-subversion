# Brisbane City Council - Intelligent Transport Systems
***
##Configuration for subversion
### Overview
Installs, configures and runs ```subversion``` 


### Supported Targets
* Ubuntu 14.04

### Files Managed
```/etc/subversion/subversion.properties```

